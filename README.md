# Layout Builder Block Rebuild

This module provides a way for rebuilding individual blocks instead of the entire layout builder element. This makes the experience of using Layout Builder seem a little more responsive.
