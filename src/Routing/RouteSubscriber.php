<?php

namespace Drupal\layout_builder_block_rebuild\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\layout_builder_block_rebuild\Form\AddBlockForm;
use Drupal\layout_builder_block_rebuild\Form\RemoveBlockForm;
use Drupal\layout_builder_block_rebuild\Form\UpdateBlockForm;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route_maps = [
      'layout_builder.add_block' => AddBlockForm::class,
      'layout_builder.remove_block' => RemoveBlockForm::class,
      'layout_builder.update_block' => UpdateBlockForm::class,
    ];
    // Switch out our custom forms.
    foreach ($route_maps as $route => $class) {
      if ($route = $collection->get($route)) {
        $route->setDefault('_form', $class);
      }
    }
  }

}
